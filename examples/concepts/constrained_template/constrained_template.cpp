#include <iostream>
#include <string>

// Define a constraint that requires a T that works with std::cout
template<typename T>
concept CanCout = requires(T t) {
    std::cout << t;
};

// The template function Print constrains T to CanCout
template<CanCout T>
void Print(T& t) {
    std::cout << t << std::endl;
}

// Example class that does not work with std::cout and thus cannot be used with Print
class Unprintable {};

int main() {
    // std::string that can be used with std::cout
    const std::string str{"A const string that can be printed."};
    // All good so far
    Print(str);

    // Construct a u that cannot be used with std::cout
    Unprintable u;
    // Compiler error! (Comment out the next line to get a valid build.)
    Print(u);

    return 0;
}

