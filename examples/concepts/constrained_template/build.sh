#!/bin/bash

if [[ -d "build" ]]; then
    cd build
    make clean
    cd ..
else
    mkdir -p build
fi

cd build 
cmake .. && make

FILE="constrained_template"
if [ -f "$FILE" ]; then
    echo "$FILE built in build/ directory."
fi
