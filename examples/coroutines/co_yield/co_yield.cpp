/**
 * This file was written following "Your first coroutine", accessible here:
 * https://blog.panicsoftware.com/your-first-coroutine/
 */

#include <iostream>
#include <cassert>
#include <experimental/coroutine>

class Resumable{
public:
    struct promise_type;
    using coro_handle = std::experimental::coroutine_handle<promise_type>;
    Resumable(coro_handle handle) : m_handle(handle) { assert(handle); }
    Resumable(const Resumable&) = delete;
    Resumable(Resumable&&) = delete;
    ~Resumable() { m_handle.destroy(); }

    bool resume() {
        if (!m_handle.done()) {
            m_handle.resume();
        }
        return !m_handle.done();
    }

    std::string recent_value();

    std::string m_yield_value;
private:
    coro_handle m_handle;
};

struct Resumable::promise_type {
    std::string value;
    using coro_handle = std::experimental::coroutine_handle<promise_type>;

    auto get_return_object() { return coro_handle::from_promise(*this); };
    auto initial_suspend() { return std::experimental::suspend_always(); };
    auto final_suspend() { return std::experimental::suspend_always(); };
    void return_void() {};
    void unhandled_exception() { std::terminate(); };
    auto yield_value(std::string v) {
        value = std::move(v);
        return std::experimental::suspend_always();
    }
};

std::string Resumable::recent_value() { return m_handle.promise().value; }

Resumable foo(){
    std::string s;
    while (true) {
        s.append("a");
        co_yield s;
    }
}

int main() {
    auto res = foo();
    std::size_t count = 0;
    while (count++ < 10) {
        res.resume();
        std::cout << res.recent_value() << std::endl;
    }

    return 0;
}

