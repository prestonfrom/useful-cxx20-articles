// With this example, we will use a coroutine to start a "task" with std::async.
// The resumable returned from the coroutine is templated on the return type that
// the future is templated on (so if std::async returns std::future<int>, the 
// resumable class will be resumable<int>.
// This example assumes everything works correctly and does not provide any error 
// handling for the sake of simplicity.
//
// This example also illustrates how C++20 coroutines act like state machines
// that are highly configurable. 
//
// If you actually need/want this type of functionality, it's probably better to
// use a proper library like https://github.com/lewissbaker/cppcoro.

#include <iostream>
#include <cassert>
#include <experimental/coroutine>
#include <thread>
#include <future>

// This isn't really necessary, but provides an combined example of coroutines and concepts
template<typename T>
concept NotVoid = requires (T t){
    !std::is_void_v<T>;
};

// Templated class that will be the "resumable" return type from a coroutine.
template<NotVoid T>
class resumable{
public:
    struct promise_type {
        T m_return_value;
        using coro_handle = std::experimental::coroutine_handle<promise_type>;

        auto get_return_object() { return coro_handle::from_promise(*this); };

        // We do not want to suspend when the coroutine starts since we want to
        // call std::async and *then* suspend.
        auto initial_suspend() { return std::experimental::suspend_never(); };
        auto final_suspend() { return std::experimental::suspend_always(); };

        // This would be a bad idea for large types that are expensive to copy or that have
        // a deleted copy constructor, but for  the current example using an int, it's okay.
        T return_value(T value) { m_return_value = std::move(value); return m_return_value; };

        // If there is an exception, we will just stop everything.
        void unhandled_exception() { std::terminate(); };
    };

    using coro_handle = std::experimental::coroutine_handle<promise_type>;

    resumable(coro_handle handle) : m_handle(handle) { assert(m_handle); }
    resumable(const resumable&) = delete;
    resumable(resumable&&) = delete;
    ~resumable() { m_handle.destroy(); }

    bool resume() {
        if (!m_handle.done()) {
            m_handle.resume();
        }
        return !m_handle.done();
    }

    // Again, this is not great if you're using an expensive to copy (or copy-deleted) class.
    T return_val() { return m_handle.promise().m_return_value; };
private:
    coro_handle m_handle;
};

// This is our actual coroutine. When called, we will start the lambda in another thread
// created by std::async so it runs in parallel to the calling function.
auto run_async_task(std::size_t loop_count) -> resumable<int> {
    using namespace std::chrono_literals;
    auto result = std::async(
        std::launch::async,
        [loop_count]() {
            std::size_t count = 0;
            while (count++ < loop_count) {
                std::this_thread::sleep_for(100ms);
                std::cout << "ASYNC IN COROUTINE: Count " << count << std::endl;
            }

            return 42;
        }
    );

    // Now that the std::async lambda has started, we can suspend the coroutine
    // and resume the calling function.
    co_await std::experimental::suspend_always();

    // Once "resume()" is called on the resumable object, we'll want to wait on the 
    // future returned from std::async until it has finished.
    result.wait();
    // Then we can return the result held by the future.
    // (Again, we're assuming the future has a valid value to return.)
    co_return result.get();
}

// This function will call the coroutine and when the coroutine suspends, it will
// perform some other operation in a loop. When the loop finishes, we'll wait for the
// coroutine to finish and then get the return value from the coroutine and return it.
auto do_async_stuff() -> int {
    using namespace std::chrono_literals;
    auto task = run_async_task(12);

    auto count = 0;
    while (count++ < 20) {
        std::this_thread::sleep_for(100ms);
        // This message should be interleaved with the messages output by the coroutine.
        std::cout << "NON-ASYNC: Count " << count << std::endl;
    }

    // Loop until the resumable is no longer resuming.
    while(task.resume());

    // Get the final value from the coroutine.
    auto result = task.return_val();

    return result;
}

int main() {
    std::cout << do_async_stuff() << std::endl;

    return 0;
}
