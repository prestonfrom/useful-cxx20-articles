/**
 * This file was written following "Your first coroutine", accessible here:
 * https://blog.panicsoftware.com/your-first-coroutine/
 */

#include <iostream>
#include <cassert>
#include <experimental/coroutine>

class Resumable{
public:
    struct promise_type;
    using coro_handle = std::experimental::coroutine_handle<promise_type>;
    Resumable(coro_handle handle) : m_handle(handle) { assert(handle); }
    Resumable(const Resumable&) = delete;
    Resumable(Resumable&&) = delete;
    ~Resumable() { m_handle.destroy(); }

    bool resume() {
        if (!m_handle.done()) {
            m_handle.resume();
        }
        return !m_handle.done();
    }
    std::string return_val();
private:
    coro_handle m_handle;
};

struct Resumable::promise_type {
    std::string m_return_value;
    using coro_handle = std::experimental::coroutine_handle<promise_type>;

    auto get_return_object() { return coro_handle::from_promise(*this); };
    auto initial_suspend() { return std::experimental::suspend_always(); };
    auto final_suspend() { return std::experimental::suspend_always(); };
    void return_value(std::string value) { m_return_value = std::move(value); };
    void unhandled_exception() { std::terminate(); };
};
std::string Resumable::return_val() { return m_handle.promise().m_return_value; }

Resumable foo(){
    std::string s{"I am the return value."};
    std::experimental::suspend_always();
    co_return s;
}

int main() {
    auto res = foo();
    while (res.resume());
    std::cout << res.return_val() << std::endl;

    return 0;
}

