#include <iostream>
#include <string>
#include <compare>

enum class KeyType {
    HARD,
    SOFT,
    ETHER
};

class UnnamedKey {
    public:
        UnnamedKey(KeyType type, uint64_t number)
            : m_type(type), m_number(number)
        {}

        // Compiler knows how to do this comparison by default because all private member variables
        // have default comparison.
        auto operator<=>(const UnnamedKey& rhs) const = default;

    private:
        // Comparison is based on the order of private member variables, 
        // so changing order of private members will change ordering.
        KeyType m_type;
        uint64_t m_number;
};

class NamedKey {
    public:
        NamedKey(std::string name, KeyType type, uint64_t number)
            : m_name(std::move(name)), m_type(type), m_number(number)
        {}

        // We need to define both primary operators (== and <=>) because the compiler does not know
        // how to default comparison for strings.
        auto operator==(const NamedKey& rhs) const {
            return m_name == rhs.m_name && m_type == rhs.m_type && m_number == rhs.m_number;
        }

        auto operator<=>(const NamedKey& rhs) const {
            // Use std::string compare to get the result of the three-way compare:
            // Less than 0 for m_name is lass than rhs.m_name
            // 0 for m_name equals rhs.m_name
            // Greater than 0 for m_name is greater than rhs.m_name
            if (auto result = m_name.compare(rhs.m_name); result != 0) {
                // Need to return the expected ordering type, so convert as appropriate and return.
                if (result < 0) {
                    return std::strong_ordering::less;
                } else {
                    return std::strong_ordering::greater;
                }
            }

            if (auto result = (m_type <=> rhs.m_type); result != 0) {
                return result;
            }

            return m_number <=> rhs.m_number;
        }

    private:
        std::string m_name;
        KeyType m_type;
        uint64_t m_number;

};

template<typename T>
auto compare_keys(const T& first_key, const T& second_key) {
    if (first_key == second_key) {
        std::cout << "The keys match!" << std::endl;
    } else if (first_key < second_key) {
        std::cout << "First key preceeds second key." << std::endl;
    } else if (second_key < first_key) {
        std::cout << "Second key preceeds first key." << std::endl;
    }

}

int main() {
    std::cout << "Comparing using defaulted <=> operator" << std::endl;
    compare_keys(UnnamedKey{KeyType::ETHER, 1}, UnnamedKey{KeyType::SOFT, 1'000'000});
    compare_keys(UnnamedKey{KeyType::ETHER, 1}, UnnamedKey{KeyType::ETHER, 1});
    compare_keys(UnnamedKey{KeyType::ETHER, 10}, UnnamedKey{KeyType::ETHER, 1});
    compare_keys(UnnamedKey{KeyType::ETHER, 10}, UnnamedKey{KeyType::ETHER, 100});
    compare_keys(UnnamedKey{KeyType::SOFT, 1'000'000}, UnnamedKey{KeyType::ETHER, 1});

    using namespace std::string_literals;
    std::cout << std::endl << "Comparing using explicit <=> operator" << std::endl;
    compare_keys(NamedKey{"A"s, KeyType::SOFT, 1'000'000}, NamedKey{"B"s, KeyType::SOFT, 1'000'000});
    compare_keys(NamedKey{"B"s, KeyType::SOFT, 1'000'000}, NamedKey{"B"s, KeyType::SOFT, 1'000'000});
    compare_keys(NamedKey{"B"s, KeyType::SOFT, 1'000'000}, NamedKey{"B"s, KeyType::SOFT, 1'000});
    compare_keys(NamedKey{"B"s, KeyType::ETHER, 1'000'000}, NamedKey{"B"s, KeyType::SOFT, 1'000'000});
    compare_keys(NamedKey{"C"s, KeyType::SOFT, 1'000'000}, NamedKey{"B"s, KeyType::SOFT, 1'000'000});

    return 0;
}
