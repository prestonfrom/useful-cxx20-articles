export module sumthing;

export class Sumthing {
public:
    auto Add(int num) -> void {
        m_sum += num;
    }

    auto GetSum() const -> int {
        return m_sum;
    }

private:
    int m_sum{0};
};
