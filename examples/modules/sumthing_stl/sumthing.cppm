module;

#include <iostream>

export module sumthing;

export class Sumthing {
public:
    auto Add(int num) -> void {
        m_sum += num;
    }

    auto GetSum() const -> int {
        return m_sum;
    }

    auto PrintSum() const -> void {
        std::cout << "Total amount summed: " << m_sum << std::endl;
    }

private:
    int m_sum{0};
};
