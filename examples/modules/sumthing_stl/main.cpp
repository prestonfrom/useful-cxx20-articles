#include <iostream>

// If the include for iostream comes after the import for sumthing, 
// it results in a redeclaration. I think this would be fixed by using
// Standard Library Modules when available.
import sumthing;

int main() {
    std::cout << "Let's add some things!" << std::endl;
    int a{1};
    int b{2};

    Sumthing s;

    s.Add(a);
    s.Add(b);

    s.PrintSum();

    return 0;
}
