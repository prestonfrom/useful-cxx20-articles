# A list of useful online articles on C++20 features and how to enable them

## Contents
#### [Articles](#articles)
- [Atomic Shared Pointer](#atomic_shared_ptr)
- [Comparisons](#comparisons)
- [Concepts and Requires](#concepts_and_requires)
- [Coroutines](#coroutines)
- [Modules](#modules)
- [Concurrency](#concurrency)
- [Ranges](#ranges)

#### [Usage Examples](#usage_examples)
- [How to build examples](#how_to_build_examples)
- [comparison](#comparison_examples)
- [concepts](#concepts_examples)
- [coroutines](#coroutines_examples)
- [modules](#modules_examples)

## C++20 standard released!
Clang 11 now supports [`c++20` with the std flag](https://github.com/llvm/llvm-project/commit/24ad121582454e625bdad125c90d9ac0dae948c8)! [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Clang-std-cpp-20-Added) has more information.

## <a name="articles"/>Articles

### <a name="atomic_shared_ptr"/>Atomic Shared Pointer
- *NOTE:* Both atomic `shared_ptr` and `weak_ptr` are _not_ currently (Feb 5, 2020) supported by any [compilers](https://en.cppreference.com/w/cpp/compiler_support).
- [Why do we need `atomic_shared_ptr`?](https://www.justsoftwaresolutions.co.uk/threading/why-do-we-need-atomic_shared_ptr.html)
    - Explains reasons for adding atomic `shared_ptr` (better multi-threaded support being the primary reason)
    - Provides explanations of various race conditions and how that can be solved with `atomic_shared_ptr`.
- [Atomic Smart Pointers](https://www.modernescpp.com/index.php/atomic-smart-pointers)
    - Discusses how non-atomic smart pointers are and are not thread-safe
    - Discusses how to current atomic operations can be performed with `shared_ptr`
    - Provides a clear example comparing atomic operation with C++11 `shared_ptr` vs C++20 `atomic_shared_ptr`.

### <a name="comparisons"/>Comparisons
- [Comparisons in C++20](https://brevzin.github.io/c++/2019/07/28/comparisons-cpp20/)
    - Introduction to the three-way-comparison operator (or "spaceship operator") and how comparisons will work in C++20 as opposed to C++17, including defaults and reversals of operators.

### <a name="concepts_and_requires"/>Concepts and Requires
- *Note:* The concepts library is currently (Feb 3, 2020) only [supported](https://en.cppreference.com/w/cpp/compiler_support) in `GCC libstdc++ Version 10` and `MSVC Standard Library 19.23`
- To enable concepts/requires:
    - With Clang 11, only need to specify C++20 standard: `-std=c++20`
    - Unsure about older versions, but probably need: `-std=c++2a -Xclang -fconcepts-ts`
- [C++20: Concepts, the Details](https://www.modernescpp.com/index.php/c-20-concepts-the-details)
    - Discusses "three ways" to use concepts: Requires clause, trailing requires clause, and constrained template parameters
- [Requires-expression](https://akrzemi1.wordpress.com/2020/01/29/requires-expression/)
    - Discusses `requires` clauses and how to use them as well as some possible cases for errors/misunderstanding
- [C++20 Concepts](https://omnigoat.github.io/2020/01/19/cpp20-concepts/)
    - Article's own description: `A quick syntax-based overview of C++20 Concepts, as they are in the standard (circa January 2020).`
    - Provides examples using concepts, requires, and constrained auto
- [Constraints and concepts (since C++20)](https://en.cppreference.com/w/cpp/language/constraints)
    - cppreference.com's page on concepts, constraints, and requires-expressions
- [Concepts library (C++20)](https://en.cppreference.com/w/cpp/concepts)
    - From page description: `The concepts library provides definitions of fundamental library concepts that can be used to perform compile-time validation of template arguments and perform function dispatch based on properties of types. These concepts provide a foundation for equational reasoning in programs.`
    - Especially interesting are new templates that provide concept versions of older template-helper functions, such as [`std::same_as`](https://en.cppreference.com/w/cpp/concepts/same_as) vs [`std::is_same_v`](https://en.cppreference.com/w/cpp/types/is_same)
- [CppCon 2019: Saar Raz "C++20 Concepts: A Day in the Life"](https://www.youtube.com/watch?v=qawSiMIXtE4)
    - CppCon 2019 talk by Saar Raz, who implemented concepts in Clang
- [Support for C++20’s Concepts in CLion](https://blog.jetbrains.com/clion/2019/11/cpp20-concepts-in-clion/)
    - This should not be necessary for newer versions of CLion/Clang, but may be for GCC.

### <a name="coroutines"/>Coroutines
- To enable coroutines with Clang/LLVM:
    - Need to build a recent version of libc++ to get `<experimental/coroutines>`
    - Pass compiler flags to use libc++ and enable coroutines: `-std=c++20 --stdlib=libc++ -fcoroutines-ts`
- [C++ Coroutines: Understanding operator co_await](https://lewissbaker.github.io/2017/11/17/understanding-operator-co-await)
    - In-depth explanation of the `co_await` operator, which is used to get an 'awaiter' object from a coroutine.
- [Coroutine Theory](https://lewissbaker.github.io/2017/09/25/coroutine-theory)
    - By the author of the cppcoro library listed below.
    - From the article: `In this post I will describe the differences between functions and coroutines and provide a bit of theory about the operations they support. The aim of this post is introduce some foundational concepts that will help frame the way you think about C++ Coroutines.`
- [Your first coroutine](https://blog.panicsoftware.com/your-first-coroutine/)
    - Guide to implementing `coroutine_handle` and using it with `co_await`, `co_return`, and `co_yield`.
- [How C++ coroutines work](https://kirit.com/How%20C%2B%2B%20coroutines%20work)
    - A series of articles about coroutines including how they work and how to use them, including a piece on using `co_yield` to create iterators.
    - Provides good in-depth look at how and why to use `co_return`, `co_yield`, and `co_await`.
- [C++ links: Coroutines](https://gist.github.com/MattPD/9b55db49537a90545a90447392ad3aeb#file-cpp-std-coroutines-draft-md)
    - A large list of more coroutine links to check out!
- [Coroutines TS Customization Points](https://hackmd.io/@redbeard0531/S1H_loeA7?type=view)
    - A quick guide/reference to customization for coroutines -- intended for library implementation.
- [CppCoro - A coroutine library for C++](https://github.com/lewissbaker/cppcoro)
    - Description from GitHub:
        - `The 'cppcoro' library provides a set of general-purpose primitives for making use of the coroutines TS proposal described in N4680.`
    - Might be helpful to see how coroutines are used by others
- [What are coroutines in C++20?](https://stackoverflow.com/questions/43503656/what-are-coroutines-in-c20#44244451)
    - StackOverflow answer explaining what coroutines are in C++20

### <a name="modules"/>Modules
- To enable modules:
    - With Clang 11, only need to specify C++20 standard: `-std=c++20`
    - With Clang 9+, only need to specify C++2a standard: `-std=c++2a`
    - With Clang 8 probably need: `-std=c++2a -fmodules-ts`
- *NOTE:* CMake does not currently support modules, so it is necessary to build modules manually. See Usage Examples below.
- [C++20 and C++17 Examples](https://github.com/scivision/cxx20-examples)
    - Provides concrete example of exporting/importing module and building with Clang
    - NOTE: CMake does not currently support modules (as of Feb 3, 2020)
- [C++20: Modules](https://modernescpp.com/index.php/c-20-modules)
    - Introductory overview of how to use modules in C++20
- [C++20: More Details to Modules](https://modernescpp.com/index.php/c-20-more-details-to-modules) 
    - Continuation from the above article
    - Includes example for how to use standard library as modules.
- [Using C++ Modules in Visual Studio 2017](https://devblogs.microsoft.com/cppblog/cpp-modules-in-visual-studio-2017/)
    - Discusses how to use modules in Visual Studio 2017, including experimental implementation of Standard Library Modules
- [Modules (since C++20)](https://en.cppreference.com/w/cpp/language/modules)
    - cppreference.com's article on modules
- [Clang 11 Documentation -- Modules](https://clang.llvm.org/docs/Modules.html)
    - Clang/LLVM page on why modules are necessary and how to use them.
    - [See this section](https://clang.llvm.org/docs/Modules.html#includes-as-imports) for information on handling includes in modules.

### <a name="concurrency">Concurrency
- [CppCon 2019: Anthony Williams “Concurrency in C++20 and Beyond”](https://www.youtube.com/watch?v=jozHW_B3D4U)
    - Hour-long talk that goes through the new concurrency features in C++20, including std::jthread, std::latch, std::barrier, std::counting_semaphore, std::binary_semaphore, std::atomic_ref, and new additions to std::future. Also touches on atomic shared_ptr and coroutines.
- [A new thread in C++20 (jthread)](https://medium.com/@vgasparyan1995/a-new-thread-in-c-20-jthread-ebd121ae8906)
    - A quick overview of std::jthread and how to use std::stop_token to halt a thread.
- [std::future Extensions](https://www.modernescpp.com/index.php/std-future-extensions)
    - ModernesCpp article covering the updates to std::future, including `is_ready` and `then` methods.

### <a name="ranges"/>Ranges
- *NOTE:* The ranges library is not currently (Feb 3, 2020) supported by any [compilers/standard libraries](https://en.cppreference.com/w/cpp/compiler_support), but the experimental version of the library developed by the proposal author is available on GitHub (next item).
- [range-v3](https://github.com/ericniebler/range-v3)
    - From GitHub description: `Range library for C++14/17/20. This code was the basis of a formal proposal to add range support to the C++ standard library. That proposal evolved through a Technical Specification, and finally into P0896R4 "The One Ranges Proposal" which was merged into the C++20 working drafts in November 2018.`
- [Ranges library (C++20)](https://en.cppreference.com/w/cpp/ranges)
    - cppreference.com's page on the ranges library 
- [Introduction to the C++ Ranges Library](https://www.fluentcpp.com/2018/02/09/introduction-ranges-library/)
    - Fluent C++ article/video introducing ranges

# <a name="usage_examples"/>Usage Examples

Many of the above links provide code examples but not full build examples. As many features are incomplete or experimental, it is not always very straightforward to see how to actually use them, so examples containing both C++ and CMakeLists.txt files are provided.

### <a name="how_to_build_examples"/>How to build examples
All examples are intended to be built with Clang 11. This repo suggests two options to get and use Clang 11.

1. Build and run Clang 11 locally.
    1. See [Clang LLVM](https://clang.llvm.org/get_started.html) for instructions on how to build the most up-to-date version of Clang. 
    2. Coroutines require Clang's libc++ (as opposed to gnu's libstdc++). See [libc++ on LLVM](https://libcxx.llvm.org/docs/BuildingLibcxx.html) for instructions on how to build libc++.
2. Use the Dockerfile in this repo to create a container that will build/provide Clang 11.
    1. Use `./image build` to create the image. By default, this command uses podman. To specify docker, use `./image build docker`.
    2. Start the container with the command `./image run`. By default, this command uses podman. To specify docker, use `./image run docker`.
    3. Once in the container, use `cd useful-cxx20-articles/examples` to switch to the example directory.

### <a name="comparison_examples"/>comparison
- examples/comparison/spaceship
    - Example usage of three-way-comparison operator (spaceship operator) with both a defaulted and explicit use case.

### <a name="concepts_examples"/>concepts
- examples/concepts/constrained_template
    - Example usage of a constrained template that will *not* compile. Try building it using the `build.sh` script to see the type of compiler error concepts generates.

### <a name="coroutines_examples"/>coroutines
- examples/coroutines/co_await
    - Example usage of `coroutine_handle` and `co_await`, written following [Your first coroutine](https://blog.panicsoftware.com/your-first-coroutine/).
- examples/coroutines/co_return
    - Example usage of `coroutine_handle` and `co_return`, written following [Your first coroutine](https://blog.panicsoftware.com/your-first-coroutine/).
- examples/coroutines/co_yield
    - Example usage of `coroutine_handle` and `co_yield`, written following [Your first coroutine](https://blog.panicsoftware.com/your-first-coroutine/).
- examples/coroutine/async_coroutine
    - Example of a coroutine used to start an async task and then resume the coroutine to get the final result.

### <a name="modules_examples"/>modules
- examples/modules/sumthing
    - Example usage of creating, exporting and importing a module with a simple class that does not use standard library.
    - Check makefile in this directory for simple example of precompiling and linking a module.
- examples/modules/sumthing_stl
    - Example usage of creating, exporting and importing a module with a simple class that *does* use the standard library through includes (*not* imports).
    - Check makefile in this directory for simple example of precompiling and linking a module.
