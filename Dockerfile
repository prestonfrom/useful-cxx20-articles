# Using fedora
FROM fedora:latest

RUN mkdir /root/mounted

# Install all dependencies necessary to build clang
RUN dnf install git cmake3 make ninja-build gcc g++ vim -y

# Clone the latest from the llvm project so we can build it
RUN git clone https://github.com/llvm/llvm-project.git

# Create/use build directory (so we don't build in the project)
RUN cd llvm-project && \
    mkdir build && cd build && \
    # We want a release build and we'll use ninja to build.
    cmake -DLLVM_ENABLE_PROJECTS="libcxx;libcxxabi;clang" \
        -DCMAKE_BUILD_TYPE=RELEASE \
        -G "Ninja" \
        ../llvm && \
    # Build and install
    ninja && \
    ninja install && \
    ninja install-cxx install-cxxabi

ENV LD_LIBRARY_PATH="/usr/local/lib/"
